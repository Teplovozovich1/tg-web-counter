import { createSlice } from '@reduxjs/toolkit';
import { documentAPI } from '../../api/documentApi';

interface InitialState {
  value: number;
  status: string;
  documentData: null | object;
  personalAccount: number | null;
  counters: any[];
  isLoading: boolean;
  error: null | boolean;
  errorDescription: string;
}

const initialState: InitialState = {
  value: 0,
  status: 'idle',
  documentData: null,
  personalAccount: null,
  counters: [],
  isLoading: false,
  error: null,
  errorDescription: ''
};

export const counterSlice = createSlice({
  name: 'counter',
  initialState,
  reducers: {
    setPersonalAccount: (state, action) => {
      state.personalAccount = action.payload;
    },
    setCounters: (state, action) => {
      state.counters = action.payload
    },
    putCounters: (state, action) => {
    },
    setLoading: (state, action) => {
      state.isLoading = action.payload
    },
    requestFailed: (state, action) => {
      state.error = action.payload.message
      state.errorDescription = action.payload.description
    }
  }
});

export const { setPersonalAccount, setCounters, setLoading, requestFailed } = counterSlice.actions;

export const getCounterThunk = (personalAccount: number) => async (dispatch: any) => {
  dispatch(setLoading(true))

  let response = await documentAPI.getCounter(personalAccount)
  if (response.hasOwnProperty("message")) {
    dispatch(requestFailed(response))
  } else {
    dispatch(setCounters(response.counters))
  }

  dispatch(setLoading(false))
};

export const putCounterThunk = (counters: any, personalAccount: number) => async (dispatch: any) => {
  dispatch(setLoading(true))

  const hasError = counters.some((counter: any) => counter.error);
  if (!hasError) {
    const processedCounters = counters.reduce((acc: any, counter: any) => {
      acc[counter.oid] = counter.currentInputValue;
      return acc;
    }, {});

    const responseData = await documentAPI.putCounters(processedCounters, personalAccount);
  }

  dispatch(setLoading(false));
};


export default counterSlice.reducer;
