import { HttpResponse, delay, http } from "msw";
import { faker } from '@faker-js/faker';

const generateCounters = () => ({
    "account": 123456,
    "counters": [
        {
            "oid": 1111111,
            "serial": "АМ32145",
            "model": "СИЭТ-1",
            "location": "Кухня",
            "unit": "Гкал",
            "previous": {
                "value": "123.456",
                "date": "string"
            },
            "service": {
                "oid": 4,
                "name": "Центральное отопление"
            },
        },
        {
            "oid": 1111112,
            "serial": "АМ32145",
            "model": "СИЭТ-1",
            "location": "Кухня",
            "unit": "кВт.ч",
            "current": {
                "value": "623",
                "date": "string"
            },
            "previous": {
                "value": "622",
                "date": "string"
            },
            "service": {
                "oid": 1,
                "name": "Электричество"
            },
            "next_verification": "01.05.2021"
        },
        {
            "oid": 1111113,
            "serial": "АМ32145",
            "model": "СИЭТ-1",
            "location": "Кухня",
            "unit": "м3",
            "current": {
                "value": "623",
                "date": "string"
            },
            "previous": {
                "value": "123",
                "date": "string"
            },
            "service": {
                "oid": 2,
                "name": "Водичка"
            },
            "next_verification": "01.05.2021"
        }, {
            "oid": 1111114,
            "serial": "АМ32145",
            "model": "СИЭТ-1",
            "location": "Кухня",
            "unit": "м3",
            "current": {
                "value": "323",
                "date": "string"
            },
            "previous": {
                "value": "123",
                "date": "string"
            },
            "service": {
                "oid": 3,
                "name": "Водичка"
            },
            "next_verification": "01.05.2021"
        }, {
            "oid": 1111115,
            "serial": "АМ32145",
            "model": "СИЭТ-1",
            "location": "Кухня",
            "unit": "м3",
            "current": {
                "value": "623",
                "date": "string"
            },
            "previous": {
                "value": "123",
                "date": "string"
            },
            "service": {
                "oid": 2,
                "name": "Водичка"
            },
            "next_verification": "01.05.2021"
        },
        {
            "oid": 2222226,
            "serial": "АМ32145",
            "model": "СИЭТ-2",
            "location": "Туалет",
            "unit": "МВт",
            "current": {
                "value": "525.456",
                "date": "string"
            },
            "previous": {
                "value": "125.4568",
                "date": "string"
            },
            "service": {
                "oid": 4,
                "name": "Нецентральное отопление"
            },
            "next_verification": "01.05.2021"
        },
        {
            "oid": 22222426,
            "serial": "АМ32145",
            "model": "СИЭТ-2",
            "location": "Туалет",
            "unit": "м3",
            "current": {
                "value": "525",
                "date": "string"
            },
            "previous": {
                "value": "125",
                "date": "string"
            },
            "service": {
                "oid": 3,
                "name": "Холодная вода"
            },
            "next_verification": "01.05.2021"
        },
        {
            "oid": 2222227,
            "serial": "АМ32145",
            "model": "СИЭТ-2",
            "location": "Туалет",
            "unit": "м3",
            "current": {
                "value": "125",
                "date": "string"
            },
            "previous": {
                "value": "125",
                "date": "string"
            },
            "service": {
                "oid": 2,
                "name": "Горячая вода"
            },
            "next_verification": "01.05.2021"
        },
        {
            "oid": 3333338,
            "serial": "АМ32145",
            "model": "СИЭТ-3",
            "location": "Подъезд",
            "unit": "кВт.ч",
            "current": {
                "value": "129",
                "date": "string"
            },
            "previous": {
                "value": "126",
                "date": "string"
            },
            "service": {
                "oid": 1,
                "name": "Электричество"
            },
            "next_verification": "01.05.2021"
        }
    ]
});

const responseCounters = {
    "account": 123456,
    "status": true,
    "skipped": [
        1111113
    ],
    "failed": [
        1111112
    ],
    "passed": [
        1111111
    ]
};

export const handlers = [
    http.get('/api/users', (resolver) => {
        return HttpResponse.json([
            {
                id: 1,
                name: "aboba"
            }
        ])
    }),
    // http.post('api/search', async ({ request }) => {
    //     const requestBody = await request.json()
    //     const document = generateDocument();
    //     return HttpResponse.json(document, { status: 201 })
    // }),
    http.get('api/account/123456/counter/', async () => {
        await delay(500)
        return HttpResponse.json(generateCounters(), { status: 200 })
    }),
    http.put('api/account/123456/counter/', async ({ request }) => {
        await delay(1000)
        const requestBody = await request.json()
        return HttpResponse.json(responseCounters, { status: 201 })
    }),
    http.all('api/account/*/counter/', async () => {
        await delay(500)
        return HttpResponse.json(
            {
                message: 'Endpoint not found',
                description: 'This endpoint does not exist'
            }, { status: 400 })
    })
];
