import axios from "axios";

const instance = axios.create({
    baseURL: "http://localhost:3000/",
    timeout: 10000,
    headers: { 'X-Custom-Header': 'foobar' }
});

export const documentAPI = {
    searchDocument(amount: any) {
        let axiosGet = axios.post(`api/search`, amount).then(res => res.data)
        return axiosGet
    },
    getCounter(personalAccount: number) {
        return axios.get(`api/account/${personalAccount}/counter/`)
            .then(response => response.data)
            .catch(error => {
                if (error.response) {
                    return { message: error.response.data.message, description: "something" };
                } else {
                    return { error: 'Network Error' };
                }
            });
    },
    putCounters(counters: any, personalAccount: number) {
        let axiosPut = axios.put(`api/account/${personalAccount}/counter/`, counters).then(res => res.data)
        return axiosPut
    }
}

