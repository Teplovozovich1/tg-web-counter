import InputField from "./InputCounter"
import s from './Card.module.scss'

let counterIcon: Array<string> = ["", ""];

function Card({ id, onValidationChange, serviceId, serviceName, counterValue, unit, next_verification, differenceValue, setCurrentInputValue, convertedCounters, error }: any) {

  switch (serviceId) {
    case 1:
      counterIcon = ["fa-regular fa-bolt fa-lg", "electricity"];
      break;
    case 2:
      counterIcon = ["fa-regular fa-droplet fa-lg fa-flip-horizontal", "warm_water"];
      break;
    case 3:
      counterIcon = ["fa-regular fa-droplet fa-lg fa-flip-horizontal", "cold_water"];
      break;
    case 4:
      counterIcon = ["fa-regular fa-droplet fa-lg fa-flip-horizontal", "warm_water"];
      break;
    default:
      counterIcon = ["fa-regular fa-droplet fa-lg fa-flip-horizontal", "cold_water"];
      break;
  }

  return (
    <div className={s.card_box}>
      <div className={s.card_content}>
        <div className={s.first_elements}>
          <div>
            <i className={`${counterIcon[0]} ${s[counterIcon[1]]}`}></i>
          </div>
        </div>
        <div className={s.second_elements}>
          <p className={s.service_name}>{serviceName}</p>
          <ul className={s.second_element_items}>
            <li>{next_verification ? `Проверка ${next_verification}` : 'Нет проверки'}</li>
            <li>№ {id}</li>
          </ul>
        </div>
        <div className={s.third_elements}>
          <div className={s.input_box}>
            <InputField id={id} serviceId={serviceId}
              onValidationChange={onValidationChange}
              counterValue={counterValue} unit={unit}
              setCurrentInputValue={setCurrentInputValue}
              convertedCounters={convertedCounters}
              error={error}
            />
            <span className={s.text_under_input}>{unit}</span>
          </div>
          <div className={s.consumed}>
            <p>Потреблено {differenceValue} {unit}</p>
          </div>
        </div>
        {/* <div className={s.fourth_elements}>
        </div> */}
      </div>
    </div>

  );
}

export default Card;