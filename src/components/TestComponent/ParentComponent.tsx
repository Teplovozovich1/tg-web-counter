import React, { useEffect, useState } from 'react';
import s from './ParentComponent.module.scss'
import Card from './Card';
import { useDispatch, useSelector } from 'react-redux';
import { putCounterThunk } from '../../redux/reducers/counterSlice';
import { CounterDispatch } from '../../redux/store';
import { PreLoader } from '../Common/PreLoader/PreLoader';
import { PreLoaderCountersDay } from '../Common/PreLoader/PreLoaderCountersDay';

interface Counter {
  oid: number;
  serial: string;
  model: string;
  location: string;
  unit: string;
  current: { value: number; date: string };
  previous: { value: number; date: string };
  service: { oid: number; name: string };
  next_verification: string;
  error: boolean
}

interface CardValidation extends Counter {
  isValid: boolean
  currentInputValue: number
  error: boolean
}

interface InputValue {
  oid: number;
  value: string;
}

function ParentComponent({personalAccount}: any) {
  const counters = useSelector((state: any) => state.counter.counters)
  const [convertedCounters, setConvertedCounters] = useState<CardValidation[]>([])
  const [groupedCounters, setGroupedCounters] = useState<{ [key: string]: Counter[] }>({})
  const [isButtonDisabled, setButtonDisable] = useState(false)
  const [isPostcardDisabled, setPostcard] = useState(false)
  const isLoading = useSelector((state: any) => state.counter.isLoading);
  const dispatch = useDispatch<CounterDispatch>();

  useEffect(() => {
    groupCountersByLocation(counters)
    setConvertedCounters(transformedCounters)
  }, [counters])

  const transformedCounters = counters.reduce((acc: any, counter: any) => {
    const existingCounter = acc.find((c: any) => c.oid === counter.oid);
    if (!existingCounter) {
      acc.push({ ...counter, isValid: false, error: false, currentInputValue: counter.current ? counter.current.value : counter.previous.value });
    }
    return acc;
  }, []);

  const groupCountersByLocation = (counters: Counter[]) => {
    const grouped: { [key: string]: Counter[] } = {};
    counters.forEach(counter => {
      const location = counter.location;
      if (!grouped[location]) {
        grouped[location] = [];
      }
      grouped[location].push(counter);
    });
    setGroupedCounters(grouped);
  };

  const handleValidationChange = (cardId: number, isValid: boolean) => {
    setConvertedCounters(prev =>
      prev.map(card =>
        card.oid === cardId ? { ...card, isValid } : card
      )
    );
  };

  const handleButtonClick = () => {
    convertedCounters.forEach((counter) => {
      if (counter.currentInputValue < (counter.current ? counter.current.value : counter.previous.value)) {
        counter.error = true
      } else {
        counter.error = false
      }
    })
    setConvertedCounters([...convertedCounters]);

    dispatch(putCounterThunk(convertedCounters, personalAccount))

    const hasError = convertedCounters.some((counter: any) => counter.error);
    if (!hasError) {
      setPostcard(true)
    }
  };

  useEffect(() => {
    setButtonDisable(convertedCounters.some(card => !card.isValid || card.error))
  }, [convertedCounters])


  return (
    <div className={s.submit_form}>
      {isLoading && <PreLoader />}
      {isPostcardDisabled && <PreLoaderCountersDay />}

      {Object.keys(groupedCounters).map((location: string) => (
        <div className={s.card_group} key={location}>
          <h2 className={s.title}>{location}</h2>
          <div>
            {groupedCounters[location].map(counter => (
              <Card
                key={counter.oid}
                id={counter.oid}
                onValidationChange={handleValidationChange}
                location={counter.location}
                serviceName={counter.service.name}
                serviceId={counter.service.oid}
                counterValue={counter.current == null ? counter.previous.value : counter.current.value}
                differenceValue={counter.current == null ? 0 : Math.round(counter.current.value - counter.previous.value)}
                unit={counter.unit}
                next_verification={counter.next_verification}
                setCurrentInputValue={setConvertedCounters}
                convertedCounters={convertedCounters}
                error={convertedCounters.find(cc => cc.oid === counter.oid)?.error || false}
              // currentInputValue={convertedCounters.find(cc => cc.oid === counter.oid)?.currentInputValue || false}
              />
            ))}
          </div>
        </div>
      ))}
      <div className={s.space}></div>
      <button className={`${s.submit_button} ${isButtonDisabled ? s.submit_button_disable : s.submit_button}`}
        disabled={isButtonDisabled}
        onClick={handleButtonClick}>Отправить показания
      </button>
      <button onClick={() => setPostcard(false)} className={`${isPostcardDisabled == true ? s.postcard_button : s.postcard_button_disable}`} disabled={!isPostcardDisabled}>Спасибо</button>
    </div>
  );
}

export default ParentComponent;