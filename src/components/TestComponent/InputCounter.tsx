import React, { useEffect } from 'react';
import styles from './InputCounter.module.scss';
import { useFormik } from 'formik';
import * as yup from 'yup';

interface CardProps {
  id: string | number;
  onValidationChange: (id: string | number, isValid: boolean) => void;
  serviceId: number;
  counterValue: number
  unit: string
  setCurrentInputValue: any
  convertedCounters: any
  error: any
}

let minVal = 0;
let maxVal = 0;

function InputField({ id, onValidationChange, serviceId, counterValue, unit, setCurrentInputValue, convertedCounters, error }: CardProps) {

  switch (serviceId) {
    case 1:
      minVal = 1;
      maxVal = 50;
      break;
    case 3:
      minVal = 2;
      maxVal = 15;
      break;
    default:
      minVal = 1;
      maxVal = 50;
      break;
  }

  const validationSchema = yup.object({
    textInput: yup.string()
      .required('Поле обязательно для заполнения')
      .min(minVal, `Минимальное значение: ${minVal}`)
      .max(maxVal, `Максимальное значение: ${maxVal}`)
      // .test('props-error', 'Ошибка приходит из пропсов', function(value) {
      //   if (error == true) {
      //     return false;
      //   } else {
      //     return true
      //   }
      // }),
    });
  


  const handleOnChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const inputValue = e.target.value;

    const intEnableCharacters = /^[0-9\b]+$/;
    const floatUnitRegexMap: { [key: string]: RegExp } = {
      'Гкал': /^\d*\.?\d{0,4}$/,
      'МВт': /^\d*\.?\d{0,3}$/
    };

    const floatUnit = ["Гкал", "МВт"];
    const isFloatUnit = floatUnit.includes(unit)

    const regex = isFloatUnit ? floatUnitRegexMap[unit] : intEnableCharacters

    if (inputValue === '' || regex.test(inputValue)) {
      formik.setFieldValue('textInput', inputValue)
      setCurrentInputValue((prevCounters: any) =>
        prevCounters.map((counter: any) => {
          if (counter.oid === id) {
            return { ...counter, currentInputValue: Number(inputValue), error: false,}
          }
          return counter
        })
      )
    }
  };

  const formik = useFormik({
    initialValues: {
      textInput: counterValue,
    },
    validationSchema,
    validateOnMount: true,
    onSubmit: (values) => {
    },
  });

  useEffect(() => {
    onValidationChange(id, formik.isValid);
  }, [formik.isValid]);

  return (
    <div>
      <form onSubmit={formik.handleSubmit}>
        <input className={`${styles.text_input} ${formik.touched.textInput && formik.errors.textInput || error ? styles.input_error : ""}`}
          type='text'
          id={`textInput-${id}`}
          {...formik.getFieldProps('textInput')}
          onChange={handleOnChange}
        />
        {/* {formik.touched.textInput && formik.errors.textInput && (
          <div><p className={styles.error_message}>{formik.errors.textInput}</p></div>
        )} */}
      </form>
    </div>
  );
}

export default InputField;
