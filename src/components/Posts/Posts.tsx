import { useEffect, useState } from "react";

interface User {
  id: number;
  name: string;
  // Other properties if available
}

export const Posts = () => {
  const [users, setUsers] = useState<User[]>([]);
  const [content, setContent] = useState('')

  useEffect(() => {
    fetch(`/api/users`)
      .then((response) => response.json())
      .then((data: any) => {
        setUsers(data)
      })
  }, []);

  const onSendClick = (e: any) => {
    e.preventDefault();
    fetch('api/messages', {
      method: "POST", headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ content })
    })
  }

  return (
    <div>
      {users.map((user) => <div key={user.id}>{user.name}</div>)}
      <input value={content} onChange={(e) => {
        setContent(e.target.value)
      }} />
      <button onClick={onSendClick}></button>
    </div>
  )
};
