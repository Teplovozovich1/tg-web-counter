import React from 'react';
import styles from './DocumentInformation.module.css'

interface DocumentInformationProps {
    documentData: any;
}

const DocumentInformation: React.FC<DocumentInformationProps> = ({ documentData }) => {
    return (
        <div className={styles.document_container}>
            <h1>Документы</h1>
            <p>Address: {documentData.address}</p>
            <p>Account: {documentData.account}</p>
            <p>Area: {documentData.area}</p>
            <p>Category: {documentData.category}</p>
            <p>Created At: {new Date(documentData.created_at).toLocaleString()}</p>
            <p>Document: {documentData.document}</p>
            <p>Kadastr: {documentData.kadastr}</p>
            <div className={styles.owners_details}>
                <h2>Индивидуальный собственник </h2>
                <p>birth_date: {documentData.owners[0].birth_date}</p>
                <div className={styles.owners_docs}>
                    <h2>Документы собственника</h2>
                    <p>issued_at: {documentData.owners[0].document.issued_at}</p>
                    <p>name: {documentData.owners[0].document.name}</p>
                    <p>number: {documentData.owners[0].document.number}</p>
                    <p>organization: {documentData.owners[0].document.organization}</p>
                    <p>series: {documentData.owners[0].document.series}</p>
                </div>
                <p>First Name: {documentData.owners[0].first_name}</p>
                <p>Last Name: {documentData.owners[0].last_name}</p>
                <p>Patronymic Name: {documentData.owners[0].patronymic_name}</p>
                <p>Registration City: {documentData.owners[0].reg}</p>
                <p>Registration date: {documentData.owners[0].reg_at}</p>
                <p>Share: {documentData.owners[0].share}</p>
                <p>SNILS: {documentData.owners[0].snils}</p>
                <h2>Владелец организации </h2>
                <p>inn: {documentData.owners[1].inn}</p>
                <p>name: {documentData.owners[1].name}</p>
                <p>ogrn: {documentData.owners[1].ogrn}</p>
                <p>Registration City: {documentData.owners[1].reg}</p>
                <p>Registration date: {documentData.owners[1].reg_at}</p>
                <p>Share: {documentData.owners[1].share}</p>
            </div>
        </div>
    );
}

export default DocumentInformation;