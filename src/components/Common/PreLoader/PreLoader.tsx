import styles from './PreLoader.module.scss'

export const PreLoader = () => {
    return (
        <div className={styles.prelodaer}>
            <span className={styles.loader}></span>
        </div>
    )
}
