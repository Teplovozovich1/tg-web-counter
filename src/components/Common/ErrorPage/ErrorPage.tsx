import styles from './ErrorPage.module.css'


export const ErrorPage: React.FC<{ error: any }> = ({ error }) => {
    return (
      <div className={styles.error_container}>
        <div className={styles.error_item}>Error:</div>
        <div className={styles.error_item}>{error}</div>
      </div>
    );
  };
