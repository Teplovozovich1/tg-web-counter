import { ErrorMessage, Field, Formik, Form } from 'formik';
import styles from './FormCounters.module.scss'
import * as Yup from 'yup';

export const FormCounters = () => {
    return (
        <Formik
            initialValues={{ firstName: '', lastName: '', email: '' }}
            validationSchema={Yup.object({
                firstName: Yup.string()
                    .min(3, 'Неполная строка')
                    .max(15, 'Must be 15 characters or less')
                    .required('Поле не может быть пустым'),
                lastName: Yup.string()
                    .min(3, 'Неполная строка')
                    .max(20, 'Must be 20 characters or less')
                    .required('Поле не может быть пустым'),
                email: Yup.string().email('Invalid email address').required('Поле не может быть пустым'),
            })}
            onSubmit={(values, { setSubmitting }) => {
                setTimeout(() => {
                    alert(JSON.stringify(values, null, 2));
                    setSubmitting(false);
                }, 400);
            }}
        >
            <Form className={styles.counter_form}>
                <h2>Межэтажный щи</h2>

                <h3><label htmlFor="firstName">Электричество</label></h3>
                <Field className={styles.text_input} name="firstName" type="text" />
                <p className={styles.error_message}><ErrorMessage name="firstName" /></p>

                <label htmlFor="lastName">Холодная</label>
                <Field className={styles.text_input} name="lastName" type="text" />
                <p className={styles.error_message}><ErrorMessage name="lastName" /></p>

                <label htmlFor="email">Горячая</label>
                <Field className={styles.text_input} name="email" type="email" />
                <p className={styles.error_message}><ErrorMessage name="email" /></p>

                <button type="submit">Submit</button>
            </Form>
        </Formik>
    );

}
