import React, { useEffect } from "react"
import styles from './App.module.scss'
import { useDispatch, useSelector } from "react-redux"
import { getCounterThunk, setPersonalAccount } from "./redux/reducers/counterSlice"
import { AppDispatch } from "./redux/store"
import { Navigate, Route, Routes, useParams } from "react-router-dom"
import { PreLoader } from "./components/Common/PreLoader/PreLoader"
import { ErrorPage } from "./components/Common/ErrorPage/ErrorPage"
import { FormCounters } from "./components/FormCounters/FormCounters"
import ParentComponent from "./components/TestComponent/ParentComponent"

const App: React.FC = () => {
  return (
    <div className={styles.app__wrapper}>
      <div className={styles.maim__wrapper}>
        <Routes>
          <Route path="/" element={<Navigate to="api/account/*/counter" />} />
          <Route path="api/account/:account/counter" element={<MainWindow />}>
          </Route>
          <Route path="*" element={<h2>Ресурс не найден</h2>} />
        </Routes>
      </div>
    </div>
  )
}
const MainWindow: React.FC = () => {
  const { account } = useParams<{ account: string }>();
  const personalAccount = useSelector((state: any) => state.counter.personalAccount);
  const error = useSelector((state: any) => state.counter.error);
  const isLoading = useSelector((state: any) => state.counter.isLoading);
  const dispatch = useDispatch<AppDispatch>();

  useEffect(() => {
    dispatch(setPersonalAccount(account))
  }, [account, dispatch])

  useEffect(() => {
    if (personalAccount !== null) {
      dispatch(getCounterThunk(personalAccount))
    }
  }, [personalAccount, dispatch])

  if (error) {
    return (
      <div>
        <ErrorPage error={error} />
      </div>
    )
  }
  return (
    <div className={styles.main}>
      {isLoading && <PreLoader />}
      {error && <ErrorPage error={error} />}

      <h2>Account: {personalAccount}</h2>
      <ParentComponent personalAccount={personalAccount} />
    </div>
  )
}
export default App;
